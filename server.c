#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <arpa/inet.h>

#define ERROR(string) { perror(string); exit(1); }

void handlerequest(int s);
void logfile(char *ip, char *p, char *c, int code, char *pa);

static const char header_200[] = "HTTP/1.1 200 Okay\r\nContent-Type: text/html; charset=UTF-8 \r\n\r\n";
static const char header_400[] = "HTTP/1.1 400 Bad Request\r\nContent-Type: text/html; charset=UTF-8 \r\n\r\n<h1>400 Bad Request</h1>";
static const char header_404[] = "HTTP/1.1 404 Not Found\r\nContent-Type: text/html; charset=UTF-8 \r\n\r\n<h1>404 Not Found</h1>";
static const char header_501[] = "HTTP/1.1 501 Method Not Implemented\r\nContent-Type: text/html; charset=UTF-8 \r\n\r\n<h1>501 Method Not Implemented</h1>";

char *clientip;

int main(int argc, char *argv[]){

	int port = 80;

	if(argc == 2) { port = atoi(argv[1]); }
	printf("[DEBUGINFO] Port: %d\n", port);

	int sock, newsock, length, pid;
	struct sockaddr_in server, client;

	sock = socket(PF_INET, SOCK_STREAM, 0);
	if(sock < 0){ ERROR("creating socket"); }

	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(port);

	if(bind(sock, (struct sockaddr*)&server, sizeof(server)) < 0) { ERROR("binding socket"); }

	listen(sock, 5);
	length = sizeof(client);

	while(1){

		newsock = accept(sock, (struct sockaddr *)&client, &length);
		printf("[DEBUGINFO] Client-IP: %s\n", inet_ntoa(client.sin_addr));
		clientip = inet_ntoa(client.sin_addr);

		if(newsock < 0) { ERROR("accepting"); }

		pid = fork();

		if(pid < 0) { ERROR("creating process (fork)"); }

		if(pid == 0) {
			close(sock);
			handlerequest(newsock);
			printf("[DEBUGINFO] Auftrag abgeschlossen.\n\n");
			exit(0);
		} else {
			close(newsock);
		}

	}
	return 0;
}

void handlerequest(int s){

	char buffer[512];
	char *filecontent;
	char message[1024];

	int statuscode = 0;

	recv(s, buffer, 512, 0);
	printf("\n==CLIENT_REQUEST====================\n\n\n");
	printf("%s", buffer);
	printf("\n==DEBUGINFO=========================\n\n");

	printf("\n[DEBUGINFO] PID: %d\n", getpid());

	char command[10];
	strncpy(command, buffer, 4);

	printf("[DEBUGINFO] Anforderung: %s\n", command);

	char split[] = " ";
	char *token;

	token = strtok(buffer, split);
	token = strtok(NULL, split);

	char path[] = ".";
	strcat(path, token);

	if(strcmp(command, "GET ") == 0){

		printf("[DEBUGINFO] GET requested!\n");

		FILE *htmlfile;
		if(strcmp(path, "./") == 0){
			strcat(path, "index.html");
		}

		printf("[DEBUGINFO] Path: %s\n", path);

		if(htmlfile = fopen(path, "r")) {
			long filesize;
			fseek(htmlfile, 0L , SEEK_END);
			filesize = ftell(htmlfile);
			rewind(htmlfile);
			filecontent = calloc(1, filesize);
			fread(filecontent, filesize, 1, htmlfile);
			fclose(htmlfile);

			//fgets(filecontent, 500, (FILE*)htmlfile);
			//fclose(htmlfile);
			printf("[DEBUGINFO] HTML-File found\n");
			//printf("[DEBUGINFO] File content: \n%s\n", filecontent);
			strcat(message, header_200);
			strcat(message, filecontent);
			statuscode = 200;
		} else {
			printf("[DEBUGINFO] HTML-File not found\n");
			strcat(message, header_404);
			statuscode = 404;
		}

	} else if(strcmp(command, "POST") == 0) {
		printf("[DEBUGINFO] POST requested\n");
		strcat(message, header_501);
		statuscode = 501;
	} else {
		printf("[DEBUGINFO] Bad Request\n");
		strcat(message, header_400);
		statuscode = 400;
	}

	write(s, message, strlen(message));

	char protocol[] = "";
	token = strtok(NULL, "\n");
	strcat(protocol, token);
	printf("[DEBUGINFO] Protokoll: %s\n", protocol);
	protocol[8] = 0;

	logfile(clientip, protocol, command, statuscode, path);
	close(s);
}

void logfile(char *ip, char *p, char *c, int code, char *pa){

	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	//IP, Datum, Uhrzeit, Protokollversion, GET/POST/..., HTTP Statuscode (200, ...), Pfad)
	FILE *log = fopen("serverlog.txt", "a");
	fprintf(log, "[LOG] %s, %d-%d-%d, %d:%d, %s, %s, %d, %s\n", ip, tm.tm_mday, tm.tm_mon+1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, p, c, code, pa);
	fclose(log);
	printf("[DEBUGINFO] Serverlog bearbeitet\n");
}

//TODO : Serverlogfile
