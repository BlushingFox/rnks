/*
 * File: client.c
 * Author: Tobias Rick
 */

//Client standard includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//Client connection includes
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
//Client functional includes
#include <unistd.h> // close function

// error treatment
void error(const char *msg) {
	perror(msg); exit(0);
}

/*
 * Application parameter declaration
 * 0 = filename
 * 1 = HTTP method
 * 2 = Host IP
 * 2 = file path
 * 3 = port (optional)
 */
int main(int argc, char *argv[]) {
	int con_port;
	char* http_method;
	char* ip_address;
	char* file_path;
	struct hostent *server;
	struct sockaddr_in serv_addr;
	int sock_con;
	int bytes;
	char* msg;
	char msg_response[4096];
	int msg_sent;
	int msg_received;
	int msg_total;
	int msg_size;

	printf("Starting client\n");

	// application parameter treatment
	if (argc == 4 || argc == 5) {
		http_method = argv[1];
		ip_address = argv[2];
		file_path = argv[3];
		printf("Set incoming HTTP method: %s\n", http_method);
		printf("Set incoming IP address: %s\n", ip_address);
		printf("Set incoming file path: %s\n", file_path);
		if (argc == 4) {
			con_port = 80;
			printf("Set standard port: 80\n");
		} else {
			con_port = atoi(argv[4]);
			printf("Set incoming port: %d\n", con_port);
		}
	} else {
		printf("False parameter input\n");
		printf("Use parameter: sudo ./RNKSClient <HTTP method> <host> <path> [<port>]\n");
		printf("Client connection aborted\n");
		return 0;
	}

	// get message size
	msg_size=0;
	if(!strcmp(http_method,"GET")) {
		msg_size+=strlen("%s %s%s%s HTTP/1.0\r\n");
	} else {
        	msg_size+=strlen("%s %s HTTP/1.0\r\n");
	}
        msg_size+=strlen(http_method);
        msg_size+=strlen(file_path);

	// message memory allocation
	msg=malloc(msg_size);

	// fill in the parameters
	if(!strcmp(http_method,"GET")) {
		sprintf(msg,"%s %s HTTP/1.0\r\n", strlen(http_method)>0?http_method:"GET", strlen(file_path)>0?file_path:"/");
	} else {
		sprintf(msg,"%s %s HTTP/1.0\r\n", strlen(http_method)>0?http_method:"POST", strlen(file_path)>0?file_path:"/");  
	}
	strcat(msg,"\r\n");

	// Request overview
	printf("\nRequest:\n%s\n", msg);

	// socket creation and error treatment
	sock_con = socket(AF_INET, SOCK_STREAM, 0);
    	if (sock_con < 0)
		error("ERROR opening socket");
	printf("Socket creation succesful\n");

	// ip address and error treatment
	server = gethostbyname(ip_address);
	if (server == NULL)
		error("ERROR, no such host");
	printf("IP address found\n");

	// Structure fill
	memset(&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(con_port);
	memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);

	// Socket connection and error treatment
    	if (connect(sock_con,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
		error("ERROR connecting");
	printf("Socket connection successful\n");

	// request sending
	msg_total = strlen(msg);
	msg_sent = 0;
	do {
		bytes = write(sock_con,msg+msg_sent,msg_total-msg_sent);
		if (bytes < 0)
			error("ERROR writing message to socket");
		if (bytes == 0)
            		break;
        	msg_sent+=bytes;
	} while (msg_sent < msg_total);

    	// get response
	memset(msg_response,0,sizeof(msg_response));
	msg_total = sizeof(msg_response)-1;
	msg_received = 0;
	do {
		bytes = read(sock_con,msg_response+msg_received,msg_total-msg_received);
		if (bytes < 0)
			error("ERROR reading response from socket");
		if (bytes == 0)
			break;
		msg_received+=bytes;
	} while (msg_received < msg_total);

	if (msg_received == msg_total)
		error("ERROR storing complete response from socket");

	// closing client
	printf("Client closed\n");
    	close(sock_con);

	// response overview
	printf("\nResponse:\n%s\n",msg_response);

	return 0;
}