/**
 * @file sender.c
 * @ingroup service
 * @brief XDT layer sender logic
 */

/**
 * @addtogroup service
 * @{
 */

// class imports
#include "sender.h"
#include "service.h"
#include <stdlib.h>
#include <stdio.h>

// timer constants
#define QUANTUM_1 151 // timer id - ack await fail
#define QUANTUM_2 152 // timer id - DT send fail
#define QUANTUM_3 153 // timer id - connection fail
#define QUANTUM_4 154 // timer id - bufflock await ack
#define QUANTUM_1_MAX 3.0 // timer max - ack await fail
#define QUANTUM_2_MAX 3.0 // timer max - DT send fail
#define QUANTUM_3_MAX 10.0 // timer max - connection fail
#define QUANTUM_4_MAX 0.5 // timer max - bufflock await ack

// buffer size
#define N 12

// sequence constants
#define expRequ 1

/* while running sender works*/
static int running = 1;

/* states and current state */
static enum states { IDLE, AWAIT_ACK, SLOWSTART, CONGAVOID, GO_BACK_N } currentState;

// timer declaration
XDT_timer q1; // timer - ack await fail
XDT_timer q2; // timer - DT send fail
XDT_timer q3; // timer - connection fail
XDT_timer q4; // timer - bufflock await ack

// buffer declaration
XDT_pdu buffer[N]; // GoBackN buffer
unsigned buffLock = 0; // buffer locker
unsigned buffIn = 0; // buffer start position
unsigned buffOut = 0; // buffer end position
unsigned buffDist = 0; // buffer start end distance

/*
 * slow start variables
 *
 * congestion window : if ACK then cgwnd++
 * slow start threshold : if cgwnd >= ssthresld then cgwnd = cgwnd + 1 / cgwnd
 * mode: 0 = slowstart, 1 = congavoid
 * cgwnd_lin: counter for congavoid mode; cgwnd_lin == cgwnd -> cgwnd++, cgwnd_lin = 0
 *
 *
 */
unsigned cgwnd = 1;
unsigned cgwnd_lin = 0;
unsigned ssthresld = 8;
unsigned mode = 0;
unsigned waitingConfSeq = 0;
unsigned waitingConf = 0;

/*
 * methods declaration
 */
static void fromIdle(void);
static void fromAwaitACK(void);
static void fromSlowStart(void);
static void fromCongAvoid(void);
static void fromGoBackN(void);
void sendDT(XDT_sdu * sdu);
void sendXABORTind(XDT_pdu * pdu);
void sendTimerXABORTind(void);
void sendXBREAKind(XDT_sdu * sdu);
void sendACKXDATconf(XDT_pdu * pdu);
void sendREQXDATconf(XDT_sdu * sdu);
void sendWaitingXDATconf(XDT_pdu * pdu);
void sendXDISindL(XDT_pdu * pdu);
int writePDUToBuffer(XDT_sdu * msg);
void deletePDUFromBuffer(XDT_pdu * pdu);
int handleBuffer(int i);
void handleCgwnd(void);

/*
 * state handle methods
 */
static void fromIdle(){
  printf("fromIdle... START\n");
  XDT_message msg;
  get_message(&msg);

  // IDLE -> AWAIT_ACK
  if(msg.type == XDATrequ){
    XDT_sdu* sdu = &msg.sdu;
    if(sdu -> x.dat_requ.sequ == expRequ){

      sendDT(sdu);
      set_timer(&q1, QUANTUM_1_MAX);

      currentState = AWAIT_ACK;

      printf("IDLE -> AWAIT_ACK\n");
    } else {
      printf("Sequence number error: connection aborting\n");

      running = 0;
    }
  }
} /* fromIdle */

static void fromAwaitACK(){
  printf("fromAwaitACK... START\n");
  XDT_message msg;
  get_message(&msg);

  // AWAIT_ACK -> SlowStart
  if(msg.type == ACK){
    XDT_pdu* pdu = &msg.pdu;
    if(pdu->x.ack.sequ == 1){
      reset_timer(&q1);
      delete_timer(&q1);

      sendACKXDATconf(pdu);

      currentState = SLOWSTART;

      cgwnd++;
      printf("AWAIT_ACK -> SLOWSTART\n");
    }
  // AWAIT_ACK -> IDLE
  } else if(msg.type == QUANTUM_1){
    XDT_pdu* pdu = &msg.pdu;
    printf("Timeout awaiting ack: connection aborting\n");

    reset_timer(&q1);
    delete_timer(&q1);

    sendXABORTind(pdu);

    currentState = IDLE;

    running = 0;
  }
} /* fromAwaitACK */

static void fromSlowStart(){
  XDT_message msg;
  get_message(&msg);

  // ACK -> cgwnd++ -> SLOWSTART
  // cgwnd >= ssthresld -> CONGAVOID
  // t2 -> SLOWSTART
  // Buffer voll -> GO_BACK_N

  if(msg.type == ACK){
    XDT_pdu* pdu = &msg.pdu;

    sendACKXDATconf(pdu);
    deletePDUFromBuffer(pdu);

    // Resort buffer
    handleCgwnd();
    cgwnd++;
    if(cgwnd == ssthresld){
      printf("\nSLOWSTART -> CONGAVOID\n");
      currentState = CONGAVOID;
      mode = 1;
    }

    reset_timer(&q2);
    set_timer(&q2, QUANTUM_2_MAX);

    reset_timer(&q3);
    set_timer(&q3, QUANTUM_3_MAX);

  } else if(msg.type == XDATrequ){
    XDT_sdu* sdu = &msg.sdu;

    buffLock = writePDUToBuffer(sdu);
    sendDT(sdu);

    if(buffLock == 1){
      printf("\nSLOWSTART -> GO_BACK_N\n");
      waitingConf = 1;
      currentState = GO_BACK_N;
      set_timer(&q4, QUANTUM_4_MAX);
    } else {
      sendREQXDATconf(sdu);
    }

  } else if(msg.type == ABO){
    XDT_pdu* pdu = &msg.pdu;

    sendXABORTind(pdu);
    printf("\nSLOWSTART -> IDLE\n");
    currentState = IDLE;
    running = 0;

  } else if(msg.type == QUANTUM_2){

    cgwnd = 1;
    printf("\nReset CGWND\n");

  } else if(msg.type == QUANTUM_3){
    sendTimerXABORTind();
    reset_timer(&q3);

    currentState = IDLE;

    running = 0;
  }

} /* fromSlowStart */

static void fromCongAvoid(){
  XDT_message msg;
  get_message(&msg);

  // ACK -> cgwnd_lin++, if cgwnd_lin == cgwnd then cgwnd++ cgwnd_lin = 0
  // t2 -> SLOWSTART
  // Buffer voll -> GO_BACK_N

  if(msg.type == ACK){
    XDT_pdu* pdu = &msg.pdu;

    sendACKXDATconf(pdu);
    deletePDUFromBuffer(pdu);

    cgwnd_lin++;
    if(cgwnd_lin == cgwnd){
      // Resort buffer
      handleCgwnd();
      cgwnd++;
      cgwnd_lin = 0;

      if(cgwnd > N){
        cgwnd = N/2;
      }
    }

    reset_timer(&q2);
    set_timer(&q2, QUANTUM_2_MAX);


  } else if(msg.type == XDATrequ){
    XDT_sdu* sdu = &msg.sdu;

    buffLock = writePDUToBuffer(sdu);
    sendDT(sdu);

    if(buffLock == 1){
      printf("\nCONGAVOID -> GO_BACK_N\n");
      waitingConf = 1;
      currentState = GO_BACK_N;
      set_timer(&q4, QUANTUM_4_MAX);
    } else {
      sendREQXDATconf(sdu);
    }

  } else if(msg.type == ABO){
    XDT_pdu* pdu = &msg.pdu;

    sendXABORTind(pdu);
    printf("\nCONGAVOID -> IDLE\n");
    currentState = IDLE;
    running = 0;

  } else if(msg.type == QUANTUM_2){
    printf("\nCONGAVOID -> SLOWSTART\n");
    currentState = SLOWSTART;
    mode = 0;

    reset_timer(&q2);
    set_timer(&q2, QUANTUM_2_MAX);
  }

} /* fromCongAvoid */

void fromGoBackN(){
  XDT_message msg;
  get_message(&msg);

  if(msg.type == ACK) {
    XDT_pdu* pdu = &msg.pdu;

    sendACKXDATconf(pdu);
    deletePDUFromBuffer(pdu);
    if(waitingConf) {
      sendWaitingXDATconf(pdu);
      waitingConf = 0;
    }
    // Ack mode handling
    if(mode){
      // Congavoid ack
      cgwnd_lin++;
      if(cgwnd_lin == cgwnd){
        // Resort buffer
        handleCgwnd();
        cgwnd++;
        cgwnd_lin = 0;

        if(cgwnd > N){
          cgwnd = N/2;
        }
      }
      printf("\nGO_BACK_N -> CONGAVOID\n");
      currentState = CONGAVOID;
    } else {
      // SlowStart ack
      // Resort buffer
      handleCgwnd();
      cgwnd++;
      if(cgwnd == ssthresld){
        printf("\nGO_BACK_N -> CONGAVOID\n");
        currentState = CONGAVOID;
        mode = 1;
      } else {
        printf("\nGO_BACK_N -> SLOWSTART\n");
        currentState = SLOWSTART;
      }
    }

    reset_timer(&q4);

  } else if(msg.type == QUANTUM_4){

    int i = 0;

    while (handleBuffer(i) != 1) i++;

    // SLOWSTART if mode = 0
    // CONGAVOID if mode = 1
    if(mode){
      printf("\nGO_BACK_N -> CONGAVOID\n");
      currentState = CONGAVOID;
    } else {
      printf("\nGO_BACK_N -> SLOWSTART\n");
      currentState = SLOWSTART;
    }

    reset_timer(&q4);

  }

} /* fromGoBackN */

/*
 * buffer handle methods
 */
int writePDUToBuffer(XDT_sdu * sdu){
  printf("writePDUToBuffer... START\n");
  XDT_pdu pdu;

  // Increase buffer start end distance
  buffDist++;

  waitingConfSeq = sdu->x.dat_requ.sequ;

  printf("writePDUToBuffer... SETTING DATA\n");
  pdu.type = DT;
  pdu.x.dt.code = DT;
  pdu.x.dt.source_addr = sdu->x.dat_requ.source_addr;
  pdu.x.dt.dest_addr = sdu->x.dat_requ.dest_addr;
  pdu.x.dt.conn = sdu->x.dat_requ.conn;
  pdu.x.dt.sequ = sdu->x.dat_requ.sequ;
  pdu.x.dt.eom = sdu->x.dat_requ.eom;
  pdu.x.dt.length = sdu->x.dat_requ.length;

  printf("writePDUToBuffer... XDT_COPY_DATA\n");
  XDT_COPY_DATA(&sdu->x.dat_requ.data, &pdu.x.dt.data, sdu->x.dat_requ.length);

  printf("writePDUToBuffer... PUT IN BUFFER\n");
  buffer[buffOut] = pdu; // write PDU to buffer
  buffOut = (buffOut+1)%cgwnd;	// look into next buffer cell

  printf("writePDUToBuffer... RETURN");
  printf("buffIn=%d | ", buffIn);
  printf("buffOut=%d\n", buffOut);
  if(buffOut == buffIn){
    printf("BuffLock=1\n");
    return 1;
  } else {
    printf("BuffLock=0\n");
    return 0;
  }
}

void deletePDUFromBuffer(XDT_pdu * pdu){
  printf("deletePDUFromBuffer... START\n");

  // Decrease buffer start end distance
  buffDist--;

  // Get number of deleting buffer entries inclusive +1 last deleting entry
  unsigned breakSequ = pdu->x.ack.sequ;
  unsigned delBuff = 1;

  printf("buffIn=%d | ", buffIn);
  printf("buffOut=%d\n", buffOut);
  printf("\ndeletePDUFromBuffer... WHILE START\n");
  // Empty buffer till input sequence
  while (delBuff){
    printf("buffer[buffIn]=%d | ", buffer[buffIn].x.dt.sequ);
    printf("breakSequ=%d\n", breakSequ);
    if(buffer[buffIn].x.dt.eom){
      printf("send XDISindL\n");
      sendXDISindL(pdu);
      currentState = IDLE;
      running = 0;
    }
    if (buffer[buffIn].x.dt.sequ == breakSequ) {
      delBuff = 0;
    }
    buffIn = (buffIn+1)%cgwnd;
  }
  // reset buffer lock
  printf("Reset buffer lock\n");
  buffLock = 0;
  printf("deletePDUFromBuffer... WHILE END\n");
}

int handleBuffer(int i){
  printf("handleBuffer... START\n");
  XDT_pdu pdu;

  pdu = buffer[(buffIn+i)%cgwnd];

  send_pdu(&pdu);
  if((buffIn+i+1)%cgwnd != buffOut){
    printf("buffout=%d | (buffIn+i+1)cgwnd=%d\n", buffOut, (buffIn+i+1)%cgwnd );
    printf("handleBuffer return 0\n");
    return 0; // buffer handle incomplete
  }

  return 1; // buffer handle complete
}

void handleCgwnd() {
    printf("handleCGWND... START\n");
    XDT_pdu tempBuffer[N];

    // Temporary buffer
    int i = 0;
    while (i < buffDist) {
      tempBuffer[i] = buffer[(buffIn+i)%cgwnd];
      i++;
    }

    // Copy temporary buffer to buffer
    int j = 0;
    while (j < buffDist) {
      buffer[j] = tempBuffer[j];
      j++;
    }

    // Set buffer pointer
    buffIn = 0;
    buffOut = buffDist;
}

/*
 * message handle methods
 */
void sendDT(XDT_sdu * sdu){
  printf("sendDT... START\n");
  XDT_pdu toSend;

  toSend.type = DT;
  toSend.x.dt.code = DT;
  toSend.x.dt.conn = sdu->x.dat_requ.conn;
  toSend.x.dt.sequ = sdu->x.dat_requ.sequ;
  toSend.x.dt.eom = sdu->x.dat_requ.eom;
  toSend.x.dt.source_addr = sdu->x.dat_requ.source_addr;
  toSend.x.dt.dest_addr = sdu->x.dat_requ.dest_addr;
  toSend.x.dt.length = sdu->x.dat_requ.length;

  XDT_COPY_DATA(&sdu->x.dat_requ.data, &toSend.x.dt.data, sdu->x.dat_requ.length);

  send_pdu(&toSend);
}

// sender connection abort inquiry
void sendXABORTind(XDT_pdu * pdu){
  printf("sendXABORTind... START\n");
  XDT_sdu toSend;

  toSend.type = XABORTind;
  toSend.x.abort_ind.conn = pdu->x.ack.conn;

  send_sdu(&toSend);
}

// sender connection timeout abort inquiry
void sendTimerXABORTind(){
  printf("sendTimerXABORTind... START\n");
  XDT_sdu toSend;

  toSend.type = XABORTind;
  toSend.x.abort_ind.conn = buffer[(buffOut-1)%cgwnd].x.dt.conn;

  send_sdu(&toSend);
}

void sendXBREAKind(XDT_sdu * sdu){
  printf("XBREAKind... START\n");
  XDT_sdu toSend;

  toSend.type = XBREAKind;
  toSend.x.break_ind.conn = sdu->x.dat_requ.conn;

  send_sdu(&toSend);
} /* sendXBREAKind */

void sendACKXDATconf(XDT_pdu * pdu){
  printf("sendACKXDATconf... START\n");
  XDT_sdu toSend;

  toSend.type = XDATconf;
  toSend.x.dat_conf.conn = pdu->x.ack.conn;
  toSend.x.dat_conf.sequ = pdu->x.ack.sequ;

  send_sdu(&toSend);
}

void sendREQXDATconf(XDT_sdu * sdu){
  printf("sendREQXDATconf... START\n");
  XDT_sdu toSend;

  toSend.type = XDATconf;
  toSend.x.dat_conf.conn = sdu->x.dat_requ.conn;
  toSend.x.dat_conf.sequ = buffer[(buffOut+cgwnd-1)%cgwnd].x.dt.sequ;

  send_sdu(&toSend);
}

void sendWaitingXDATconf(XDT_pdu * pdu){
  XDT_sdu toSend;

  toSend.type = XDATconf;
  toSend.x.dat_conf.conn = pdu->x.ack.conn;
  toSend.x.dat_conf.sequ = waitingConfSeq;

  send_sdu(&toSend);
}

void sendXDISindL(XDT_pdu * pdu){
  printf("sendXDISindL... START\n");
  XDT_sdu toSend;

  toSend.type = XDISind;
  toSend.x.dis_ind.conn = pdu->x.ack.conn;

  send_sdu(&toSend);
}


/**
 * @brief State scheduler
 *
 * Calls the appropriate function associated with the current protocol state.
 */
static void run_sender(void){
  printf("run_sender\n");

  currentState = IDLE;
  // timer - creation
  create_timer(&q1, QUANTUM_1);
  create_timer(&q2, QUANTUM_2);
  create_timer(&q3, QUANTUM_3);
  create_timer(&q4, QUANTUM_4);

  // data transfer process
  while (running){
    printf("\nCGWND = %d\n", cgwnd);
    switch(currentState){
      case IDLE :           fromIdle();
                            break;
      case AWAIT_ACK :      fromAwaitACK();
                            break;
      case SLOWSTART :      fromSlowStart();
                            break;
      case CONGAVOID :      fromCongAvoid();
                            break;
      case GO_BACK_N :      fromGoBackN();
                            break;
    }
  }

  printf("Connection aborted\n");

  // timer delete
  delete_timer(&q2);
  delete_timer(&q3);
  delete_timer(&q4);
} /* run_sender */

/**
 * @brief Sender instance entry function
 *
 * After the dispatcher has set up a new sender instance
 * and established a message queue between both processes
 * this function is called to process the messages available
 * in the message queue.
 * The only functions and macros needed here are
 * - get_message() to read SDU, PDU and timer messages from the queue
 * - send_sdu() to send an SDU message to the producer,
 * - send_pdu() to send a PDU message to the receiving peer,
 * - #XDT_COPY_DATA to copy the message payload,
 * - create_timer() to create a timer associated with a message type,
 * - set_timer() to arm a timer (on expiration a timer associated message is
 *   put into the queue)
 * - reset_timer() to disarm a timer (all timer associated messages are removed
 *   from the queue)
 * - delete_timer() to delete a timer.
 */
void
start_sender(void)
{
  printf("[SENDER.C] start_sender");
  run_sender();
} /* start_sender */


/**
 * @}
 */
