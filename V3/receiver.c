/**
 * @file receiver.c
 * @ingroup service
 * @brief XDT layer receiver logic
 */

/**
 * @addtogroup service
 * @{
 */

#include "receiver.h"
#include "service.h"
#include <stdlib.h>
#include <stdio.h>


/* TimerID and max time waiting for response */
#define TIM 50
#define TMAX 15.0

/** @brief connection number of data transfer */
static unsigned conn = 0;

/* states and current state */
static enum states { IDLE, AWAIT_CORRECT, CONNECTED } currentState;

/* while running receiver works*/
static int running = 1;

/* timer */
static XDT_timer timer;

/* expected SEQ, first SEQ always 1 */
static unsigned expSequ = 1;

/* Function declarations */
static void fromIdle(void);
static void fromConnected(void);
static void fromAwaitCorrect(void);
void timeout(void);
void sendACK(XDT_pdu * pdu);
void sendXDATind(XDT_pdu * pdu);
void sendXDISind(void);

/* transitions from idle-state */
static void fromIdle(){
  XDT_message msg;
  get_message(&msg);

  // IDLE -> CONNECTED
  if(msg.type == DT){
    XDT_pdu* pdu = &msg.pdu;
    if(pdu->x.dt.sequ == expSequ){
      sendACK(pdu);
      sendXDATind(pdu);
      expSequ++;
      currentState = CONNECTED;
      printf("IDLE -> CONNECTED\n");
    }

    reset_timer(&timer);
    set_timer(&timer, TMAX);

  }

} /* fromIdle */

/* transitions from connected-state */
static void fromConnected(){
  XDT_message msg;
  get_message(&msg);

  // CONNECTED -> CONNECTED (correct SEQ)
  printf("msg->type = %ld\n", msg.type);
  if(msg.type == DT){
    XDT_pdu* pdu = &msg.pdu;
    if(pdu -> x.dt.sequ == expSequ){
      sendACK(pdu);
      sendXDATind(pdu);
      expSequ++;
      if(!(pdu -> x.dt.eom)){
        currentState = CONNECTED;
        printf("CONNECTED -> CONNECTED\n");
      } else {
        sendXDISind();
        running = 0;
        printf("EOM");
      }
    } else {
      printf("CONNECTED -> AWAIT_CORRECT\n");
      // CONNECTED -> AWAIT_CORRECT (wrong SEQ)
      currentState = AWAIT_CORRECT;
    }

    reset_timer(&timer);
    set_timer(&timer, TMAX);

  } else if(msg.type == TIM){
    // CONNECTED -> IDLE (timeout)
    printf("TIMEOUT\n");
    timeout();
  } else {
    // no DT and no TIM
    printf("NO DT NO TIMER\n");
  }
} /* fromConnected */

/* transitions from awaitcorrect-state */
static void fromAwaitCorrect(){
  XDT_message msg;
  get_message(&msg);

  printf("[AWAIT_CORRECT] ");
  // CONNECTED -> CONNECTED (correct SEQ)
  if(msg.type == DT){
    XDT_pdu* pdu = &msg.pdu;
    if(pdu -> x.dt.sequ == expSequ){
      sendACK(pdu);
      sendXDATind(pdu);
      expSequ++;
      if(!(pdu -> x.dt.eom)){
        currentState = CONNECTED;
        printf("AWAIT_CORRECT -> CONNECTED\n");
      } else {
        currentState = IDLE;
        sendXDISind();
        running = 0;
        printf("AWAIT_CORRECT -> IDLE\n");
        return;
      }
    }

    reset_timer(&timer);
    set_timer(&timer, TMAX);

  } else if(msg.type == TIM){
    // CONNECTED -> IDLE (timeout)
    printf("TIMEOUT\n");
    timeout();
  } else {
    // no TIM and no DT
    printf("NO DT NO TIMER\n");
  }
} /* fromAwaitCorrect */

/* no response while timertime */
void timeout(){
  XDT_sdu toSendS;
  XDT_pdu toSendP;

  toSendS.type = XABORTind;
  toSendS.x.abort_ind.conn = conn;

  send_sdu(&toSendS);

  toSendP.type = ABO;
  toSendP.x.abo.conn = conn;
  toSendP.x.abo.code = ABO;

  send_pdu(&toSendP);

  currentState = IDLE;
  running = 0;
} /* timeout */

/* building and sending ACK-PDU */
void sendACK(XDT_pdu * pdu){
  printf("Sending ACK... START\n");
  XDT_pdu toSend;

  printf("Sending ACK... SETTING DATA\n");
  //toSend->type = ACK;
  toSend.type = ACK;
  toSend.x.ack.code = ACK;
  toSend.x.ack.conn = conn;
  toSend.x.ack.sequ = pdu->x.dt.sequ;
  toSend.x.ack.source_addr = pdu->x.dt.dest_addr;
  toSend.x.ack.dest_addr = pdu->x.dt.source_addr;

  printf("Sending ACK... SENDING PDU\n");
  send_pdu(&toSend);
  printf("Sending ACK... END\n");
} /* sendACK */

/* building and sending XDATind-SDU from PDU in fromIDLE*/
void sendXDATind(XDT_pdu * pdu){
  printf("Sending XDATind... START\n");
  XDT_sdu toSend;

  toSend.type = XDATind;
  toSend.x.dat_ind.conn = conn;
  toSend.x.dat_ind.sequ = pdu->x.dt.sequ;
  toSend.x.dat_ind.eom = pdu->x.dt.eom;
  toSend.x.dat_ind.length = pdu->x.dt.length;

  XDT_COPY_DATA(&pdu->x.dt.data, &toSend.x.dat_ind.data, pdu->x.dt.length);

  send_sdu(&toSend);
  printf("Sending XDATind... END\n");
} /* sendXDATind */

/* building and sending XDISind-PDU */
void sendXDISind(){
  XDT_sdu toSend;

  toSend.type = XDISind;
  toSend.x.dis_ind.conn = conn;

  send_sdu(&toSend);
  printf("Sending XDISind...\n");
} /* sendXDISind */

/**Idle
 * @brief State scheduler
 *
 * Calls the appropriate function associated with the current protocol state.
 */
static void
run_receiver(void)
{
  create_timer(&timer, TIM);
  set_timer(&timer, TMAX);

  currentState = IDLE;

  while(running){
    switch(currentState){
      case IDLE :           fromIdle();
                            break;
      case CONNECTED :      fromConnected();
                            break;
      case AWAIT_CORRECT :  fromAwaitCorrect();
                            break;
    }

  }

} /* run_receiver */

/**
 * @brief Receiver instance entry function
 *
 * After the dispatcher has set up a new receiver instance
 * and established a message queue between both processes
 * this function is called to process the messages available
 * in the message queue.
 * The only functions and macros needed here are
 * - get_message() to read SDU, PDU and timer messages from the queue
 * - send_sdu() to send an SDU message to the consumer,
 * - send_pdu() to send a PDU message to the sending peer,
 * - #XDT_COPY_DATA to copy the message payload,
 * - create_timer() to create a timer associated with a message type,
 * - set_timer() to arm a timer (on expiration a timer associated message is
 *   put into the queue)
 * - reset_timer() to disarm a timer (all timer associated messages are removed
 *   from the queue)
 * - delete_timer() to delete a timer.
 *
 * @param connection the connection number assigned to the data transfer
 *        handled by this instance
 */
void
start_receiver(unsigned connection)
{
  conn = connection;
  printf("[RECEIVER.C] start_receiver with conn=%d\n", conn);
  run_receiver();
} /* start_receiver */


/**
 * @}
 */
